# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import ir
from . import stock
from . import product


def register():
    Pool.register(
        stock.Lot,
        stock.Move,
        product.Template,
        product.Product,
        ir.Date,
        module='stock_lot_fifo', type_='model')
