# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import date

from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class Date(metaclass=PoolMeta):
    __name__ = 'ir.date'

    @classmethod
    def today(cls, timezone=None):
        fifo_assign_date = Transaction().context.get('fifo_assign_date')
        if isinstance(fifo_assign_date, date):
            return fifo_assign_date
        return super().today(timezone=timezone)
